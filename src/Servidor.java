import ar.edu.unlu.poo.todoapp.modelo.IToDo;
import ar.edu.unlu.poo.todoapp.modelo.ToDo;
import ar.edu.unlu.rmimvc.RMIMVCException;

import java.rmi.RemoteException;

public class Servidor {
    public static void main(String[] args) {
        IToDo modelo = new ToDo();
        ar.edu.unlu.rmimvc.servidor.Servidor servidor = new ar.edu.unlu.rmimvc.servidor.Servidor(
                "127.0.0.1",
                40000
        );
        System.out.println("Iniciando servidor...");
        try {
            servidor.iniciar(modelo);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        } catch (RMIMVCException e) {
            throw new RuntimeException(e);
        }
    }
}
