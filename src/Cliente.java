import ar.edu.unlu.poo.todoapp.controlador.Controlador;
import ar.edu.unlu.poo.todoapp.vista.VistaConsola;
import ar.edu.unlu.rmimvc.RMIMVCException;
import ar.edu.unlu.rmimvc.cliente.IControladorRemoto;

import java.rmi.RemoteException;

public class Cliente {
    public static void main(String[] args) {
        VistaConsola vistaConsola = new VistaConsola();
        IControladorRemoto controlador = new Controlador(vistaConsola);
        ar.edu.unlu.rmimvc.cliente.Cliente cliente = new ar.edu.unlu.rmimvc.cliente.Cliente(
                // IP del cliente
                "127.0.0.1",
                // Puerto del cliente
                40001,
                // IP del servidor (tiene que coincidir con los especificados en la clase Servidor)
                "127.0.0.1",
                // Puerto del servidor
                40000
        );
        try {
            cliente.iniciar(controlador);
            vistaConsola.iniciar();
        } catch (RMIMVCException e) {
            throw new RuntimeException(e);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }
}
