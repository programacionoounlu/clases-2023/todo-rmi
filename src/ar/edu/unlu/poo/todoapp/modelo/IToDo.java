package ar.edu.unlu.poo.todoapp.modelo;

import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

import java.rmi.RemoteException;
import java.util.List;

public interface IToDo extends IObservableRemoto {
    Tarea agregarTarea(String titulo, String descripcion) throws RemoteException;

    List<ITarea> listarTareas() throws RemoteException;

    void notificar(Object evento) throws RemoteException;

    void completarTarea(String titulo) throws RemoteException;
}
