package ar.edu.unlu.poo.todoapp.modelo;

public enum Eventos {
    TAREA_COMPLETADA,
    /**
	 * Evento producido cuando se da de alta una nueva tarea
	 */
	TAREA_AGREGADA
}
