package ar.edu.unlu.poo.todoapp.modelo;

import java.rmi.RemoteException;
import java.util.List;

import ar.edu.unlu.poo.todoapp.persistencia.RepositorioDeTareas;
import ar.edu.unlu.poo.todoapp.persistencia.RepositorioEnArchivo;
import ar.edu.unlu.poo.todoapp.persistencia.RepositorioEnMemoria;
import ar.edu.unlu.rmimvc.observer.ObservableRemoto;

public class ToDo extends ObservableRemoto implements IToDo {
	private RepositorioDeTareas tareas;
	
	public ToDo() {
		this.tareas = new RepositorioEnArchivo("tareas.bin");
	}
	
	@Override
	public Tarea agregarTarea(String titulo, String descripcion) throws RemoteException {
		Tarea tarea = new Tarea(titulo, descripcion);
		this.tareas.eliminar(titulo);
		this.tareas.guardar(tarea);
		this.notificar(Eventos.TAREA_AGREGADA);
		return tarea;
	}

	@Override
	public List<ITarea> listarTareas() throws RemoteException{
		return this.tareas.listar();
	}

	@Override
	public void notificar(Object evento) throws RemoteException {
		this.notificarObservadores(evento);
	}

	@Override
	public void completarTarea(String titulo) throws RemoteException {
		ITarea tarea = this.tareas.buscarPorTitulo(titulo);
		if(tarea != null){
			((Tarea) tarea).completar();
			this.tareas.guardar(tarea);
			this.notificar(Eventos.TAREA_COMPLETADA);
		}
	}
}
