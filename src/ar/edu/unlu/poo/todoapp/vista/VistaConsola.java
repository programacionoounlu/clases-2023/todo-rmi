package ar.edu.unlu.poo.todoapp.vista;

import java.util.List;
import java.util.Scanner;

import ar.edu.unlu.poo.todoapp.controlador.Controlador;
import ar.edu.unlu.poo.todoapp.modelo.ITarea;

public class VistaConsola implements IVista{
	
	private Scanner entrada;
	
	private Controlador controlador;
	
	public VistaConsola() {
		this.entrada = new Scanner(System.in);
	}

	public void mostrarMenuPrincipal() {
		System.out.println("########################");
		System.out.println("####### TODO App #######");
		System.out.println("########################");
		System.out.println();
		System.out.println("Selecciona una opción:");
		System.out.println("a - Agregar tarea");
		System.out.println("l - Listar tareas");
		System.out.println("c - Marcar tarea como completada");
		System.out.println();
		System.out.println("x - Salir");
	}

	@Override
	public void iniciar() {
		boolean salir = false;
		while(!salir) {
			this.mostrarMenuPrincipal();
			String opcion = this.entrada.nextLine();
			switch (opcion.toLowerCase()) {
				case "a":
					this.mostrarFormularioAlta();
					break;
				case "l":
					this.mostrarTareas(this.controlador.listarTareas());
					break;
				case "c":
					this.formularioCompletarTarea(this.controlador.listarTareas());
					break;
				case "x":
					salir = true;
					System.out.println("Recuerde completar sus tareas!");
					break;
				default:
					System.out.println("Opción no válida.");
			}
		}
	}

	private void mostrarFormularioAlta() {
		this.divisor();
		System.out.println("Por favor, ingrese un título:");
		String titulo = this.entrada.nextLine();
		System.out.println("Por favor, ingrese una descripción:");
		String descripcion = this.entrada.nextLine();
		this.controlador.agregarTarea(titulo, descripcion);
	}
	
	@Override
	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}

	private void divisor(){
		System.out.println("\n-------------------------------------------------");
	}
	@Override
	public void mostrarTareas(List<ITarea> tareas) {
		this.divisor();
		System.out.println("Tareas actuales");
		if(tareas.isEmpty()){
			System.out.println("No hay tareas para mostrar");
		}else {
			int index = 1;
			for (ITarea tarea : tareas) {
				System.out.println("Tarea " + index);
				System.out.println("Título:" + tarea.getTitulo());
				System.out.println("Descripción:" + tarea.getDescripcion());
				System.out.println(tarea.isCompletada() ? "Completada" : "Pendiente");
				index++;
			}
		}
	}

	public void formularioCompletarTarea(List<ITarea> tareas){
		this.mostrarTareas(tareas);
		int cantidadTareas = tareas.size();
		if(cantidadTareas > 0) {
			boolean valido = false;
			while (!valido) {
				try {
					System.out.println("Especifique el número de tarea a completar");
					String index = this.entrada.nextLine();
					int opcion = Integer.parseInt(index);
					valido = opcion > 0 && opcion <= cantidadTareas;
					if (!valido) {
						System.out.println("Especifique un número entre 1 y " + cantidadTareas);
					}else{
						this.controlador.completarTarea(tareas.get(opcion - 1).getTitulo());
					}
				} catch (NumberFormatException ex) {
					System.out.println("Por favor, ingrese un número entero");
				}
			}
		}
	}
}
