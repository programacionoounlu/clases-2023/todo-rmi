package ar.edu.unlu.poo.todoapp.controlador;

import java.rmi.RemoteException;
import java.util.List;

import ar.edu.unlu.poo.todoapp.modelo.Eventos;
import ar.edu.unlu.poo.todoapp.modelo.ITarea;
import ar.edu.unlu.poo.todoapp.modelo.IToDo;
import ar.edu.unlu.poo.todoapp.modelo.ToDo;
import ar.edu.unlu.poo.todoapp.vista.VistaConsola;
import ar.edu.unlu.poo.utils.observer.Observable;
import ar.edu.unlu.poo.utils.observer.Observador;
import ar.edu.unlu.rmimvc.cliente.IControladorRemoto;
import ar.edu.unlu.rmimvc.observer.IObservableRemoto;

public class Controlador implements IControladorRemoto {
	private IToDo modelo;
	
	private VistaConsola vista;
	
	public Controlador(VistaConsola vista) {
		this.vista = vista;
		this.vista.setControlador(this);
	}

	public void agregarTarea(String titulo, String descripcion) {
		try {
			this.modelo.agregarTarea(titulo, descripcion);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}

	public List<ITarea> listarTareas() {
		try {
			return this.modelo.listarTareas();
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}

	public void completarTarea(String titulo) {
		try {
			this.modelo.completarTarea(titulo);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public <T extends IObservableRemoto> void setModeloRemoto(T t) throws RemoteException {
		this.modelo = (IToDo) t;
	}

	@Override
	public void actualizar(IObservableRemoto iObservableRemoto, Object evento) throws RemoteException {
		if(evento instanceof Eventos) {
			switch((Eventos) evento) {
				case TAREA_AGREGADA:
				case TAREA_COMPLETADA:
					List<ITarea> tareas = this.modelo.listarTareas();
					this.vista.mostrarTareas(tareas);
					break;
			}
		}
	}
}
