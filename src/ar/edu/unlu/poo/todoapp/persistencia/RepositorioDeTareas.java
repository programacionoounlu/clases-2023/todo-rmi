package ar.edu.unlu.poo.todoapp.persistencia;

import ar.edu.unlu.poo.todoapp.modelo.ITarea;

import java.util.List;

public interface RepositorioDeTareas {
    void guardar(ITarea tarea);

    ITarea buscarPorTitulo(String titulo);

    List<ITarea> listar();

    boolean eliminar(String titulo);
}
