package ar.edu.unlu.poo.todoapp.persistencia;

import ar.edu.unlu.poo.todoapp.modelo.ITarea;

import java.util.ArrayList;
import java.util.List;

public class RepositorioEnMemoria implements RepositorioDeTareas{
    private List<ITarea> tareas;

    public RepositorioEnMemoria(){
        this.tareas = new ArrayList<>();
    }
    @Override
    public void guardar(ITarea tarea) {
        int indice = this.indiceDeTarea(tarea.getTitulo());
        if(indice == -1) {
            this.tareas.add(tarea);
        }else{
            this.tareas.set(indice, tarea);
        }
    }

    private int indiceDeTarea(String titulo){
        int i = 0;
        boolean encontrada = false;
        String buscado = titulo.toLowerCase();
        while(!encontrada && i < this.tareas.size()) {
            if(this.tareas.get(i).getTitulo().toLowerCase().equals(buscado)) {
                encontrada = true;
            }else {
                i++;
            }
        }
        return encontrada ? i : -1;
    }
    @Override
    public ITarea buscarPorTitulo(String titulo) {
        int indice = this.indiceDeTarea(titulo);
        return indice != -1 ? this.tareas.get(indice) : null;
    }

    @Override
    public List<ITarea> listar() {
        return this.tareas;
    }

    @Override
    public boolean eliminar(String titulo) {
        int indice = this.indiceDeTarea(titulo);
        boolean encontrada = indice != -1;
        if(encontrada){
            this.tareas.remove(indice);
        }
        return encontrada;
    }
}
