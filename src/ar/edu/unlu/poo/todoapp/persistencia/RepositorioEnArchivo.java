package ar.edu.unlu.poo.todoapp.persistencia;

import ar.edu.unlu.poo.todoapp.modelo.ITarea;

import java.io.*;
import java.util.List;

public class RepositorioEnArchivo implements RepositorioDeTareas{
    private RepositorioEnMemoria cache;

    private String archivo;
    public RepositorioEnArchivo(String archivo){
        this.cache = new RepositorioEnMemoria();
        this.archivo = archivo;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(archivo));
            Object o = ois.readObject();
            while (o != null) {
                this.cache.guardar((ITarea) o);
                o = ois.readObject();
            }
        }catch (EOFException e){
        } catch (FileNotFoundException e){
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void guardar(ITarea tarea) {
        this.cache.guardar(tarea);
        this.persistir();
    }

    @Override
    public ITarea buscarPorTitulo(String titulo) {
        return this.cache.buscarPorTitulo(titulo);
    }

    @Override
    public List<ITarea> listar() {
        return this.cache.listar();
    }

    @Override
    public boolean eliminar(String titulo) {
        boolean resultado = this.cache.eliminar(titulo);
        this.persistir();
        return resultado;
    }

    private void persistir(){
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream(this.archivo));
            for (ITarea tarea: this.cache.listar()) {
                oos.writeObject(tarea);
            }
            oos.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
